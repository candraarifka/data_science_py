"""
Now we will compare, how popular between the popular between Ironman, Thor and Hulk on Twitter using Python Data Science
"""
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
import time

consumer_key = 'your_consumer_key'
consumer_secret = 'your_consumer_secret'
access_token = 'your_access_token'
access_token_secret = 'your_access_token_secret'

class listener(StreamListener):
        def on_data(self,data):
                try:
                        print(data)
                        savefile=open("your_file_path.txt","a")
                        savefile.write(data)
                        savefile.write("\n")
                        savefile.close()
                        return True
                except BaseException as e:
                        print("Failed on data", str(e))
                        time.sleep(5)
        def on_error(self,status):
                print(status)
                
auth = OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
twitterStream=Stream(auth,listener())
twitterStream.filter(track=["Ironman","Thor","Hulk"])
print("Streaming...")
