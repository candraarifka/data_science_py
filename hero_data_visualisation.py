import json
import pandas as pd
import matplotlib.pyplot as plt

raw_data={"Nama Hero":["Ironman","Thor","Hulk"],"Total Tweet":[79,1212,277]}
df=pd.DataFrame(raw_data,columns=["Nama Hero","Total Tweet"])

labels="Ironman","Thor","Hulk"
sizes=[143,4656,727]

x_pos = list(range(len(labels)))
width = 0.8
fig, ax = plt.subplots()
plt.bar(x_pos, sizes, alpha=1, color=['red', 'blue',"green"])

ax.set_ylabel('Number of tweets', fontsize=15)
ax.set_xlabel('\n Total tweets: 4769', fontsize=15)
ax.set_title("Ironman vs Thor vs Hulk: Who's winning on Twitter?\n", fontsize=15, fontweight='bold')
ax.set_xticks([p + 0.4 * width for p in x_pos])
ax.set_xticklabels(labels)
plt.grid(True,color="k")
plt.show()
print("berhasil....")
